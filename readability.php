<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$readability = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->ReadabilityAssessment($tweet);
	if (!isset($readability[$result])) {
	    $readability[$result] = 0;
	}
	$readability[$result]++;
}

unset($DatumboxAPI);

print json_encode($readability);